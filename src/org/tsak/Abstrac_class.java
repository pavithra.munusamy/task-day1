package org.tsak;

public abstract class Abstrac_class {
	
	public void browser() {
		System.out.println("Browser Launch");
	}
	
	public void url() {
		System.out.println("Instagram");
	}
	
	public abstract void username();
	
	public abstract void password();
	
	public void login() {
		System.out.println("Login");
	}

}
