package org.tsak;

public class Child extends Parent{
	
	public void property2() {
		System.out.println("property2");
	}

	public static void main(String[] args) {
		Child c=new Child();
		Parent parent=new Parent();
		c.property2();
		c.property1();
		parent.property1();
	}
}
